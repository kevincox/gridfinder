import * as gridfinder from "./lib.js";
import assert from "assert";
import canvas from "canvas";
import snapshot from "snap-shot-it";

describe("detectGrid", () => {
	Object.entries({
		"astral-journey.jpg": {
			width: 35,
		},
		"casino.jpg": {
			width: 28,
		},
		"crossroad-keep.jpg": {
			width: 29.41,
		},
		// "dragons-lair.jpg": {
		// 	width: 35,
		// },
		// "grass.jpg": {
		// 	width: 12.5,
		// },
		"forest-camp.jpg": {
			width: 64,
		},
		"irish-pub.jpg": {
			width: 70,
			heightTolerance: 140, // Wrong, Detects roof tiles.
			offYTolerance: Infinity,
		},
		"mythictable-tutorial.jpg": {
			width: 70,
		},
		"overgrown-courtyard.jpg": {
			width: 35
		},
		"floating-islands.jpg": {
			width: 17.06,
		},
		"sewers.jpg": {
			width: 140,
			timeout: 5, // Big map.
		},
		"spaceship-main-blur.jpg": {
			width: 10.66,
			tolerance: 0.02,
			offTolerance: 10, // Wrong.
		},
		"spaceship-main-sharp.jpg": {
			width: 10.66,
			tolerance: 0.02,
			offTolerance: 5, // Wrong.
		},
		"village.jpg": {
			width: 28,
		},
		"water-temple-low.jpg": {
			width: 32,
			height: 16, // Wrong, edge detection has too many false positives.
		},
		"water-temple-medium.jpg": {
			width: 32,
		},
		"water-temple-high.jpg": {
			width: 32,
		},
		// "xak-tsaroth.jpg": {
		// 	width: 35,
		// },
	}).forEach(([map, cfg]) => {
		it(map, async function() {
			let {
				width,
				height=width,
				tolerance=0.01,
				widthTolerance=tolerance,
				heightTolerance=tolerance,

				offX=0,
				offY=0,
				offTolerance=2,
				offXTolerance=offTolerance,
				offYTolerance=offTolerance,

				timeout=1,
			} = cfg;

			this.timeout(timeout * 1000);

			let img = await canvas.loadImage(`maps/${map}`);

			let c = canvas.createCanvas(img.width, img.height)
			let ctx = c.getContext('2d');

			ctx.drawImage(img, 0, 0, img.width, img.height);

			let data = ctx.getImageData(0, 0, c.width, c.height);

			let {cellSize, offset} = gridfinder.detectGrid(data);
			console.log({cellSize, offset});

			assert(Math.abs(cellSize.width/width - 1) < widthTolerance, `Expected width ${width} got ${cellSize.width}`);
			assert(Math.abs(cellSize.height/height - 1) < heightTolerance, `Expected height ${height} got ${cellSize.height}`);
			assert(Math.abs(offset.x - offX) < offXTolerance, `Expected offX ${offX} got ${offset.x}`);
			assert(Math.abs(offset.y - offY) < offYTolerance, `Expected offY ${offY} got ${offset.y}`);
		})
	})
})
