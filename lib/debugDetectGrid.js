import * as detectGrid from "./detectGrid.js";
import * as util from "./util.js";

class ColorRange {
	constructor(values) {
		this.min = values.reduce((a, b) => Math.min(a, b));
		let max = values.reduce((a, b) => Math.max(a, b));
		this.range = max - this.min;
	}

	cssGrey(value) {
		return `hsl(0,0%,${(value-this.min)/this.range*100}%)`;
	}

	cssColor(value) {
		return `hsl(${(value-this.min)/this.range*240},100%,50%)`;
	}
}


function drawGrey({x, y, val}) {
	this.ctx.fillStyle = this.range.cssGrey(val);
	this.ctx.fillRect(x, y, 1, 1);
}

function processor(self, canvas) {
	self.canvas = canvas;
	self.ctx = canvas.getContext("2d");
	let data = self.ctx.getImageData(0, 0, self.canvas.width, self.canvas.height);
	self.ctx.clearRect(0, 0, self.canvas.width, self.canvas.height);

	let points = self.points(data);
	console.log(points);
	self.range = new ColorRange(points.map(p => p.val));

	if (self.initCtx) self.initCtx();
	for (let p of points) {
		self.draw(p);
	}
}

export function rowQuality(...args) {
	return processor({
		points(data) {
			return detectGrid.scoreRows(data)
				.map((val, y) => ({y, val}))
				.filter(({v}) => v != 0);
		},

		initCtx() {
			this.ctx.setLineDash([20, 80]);
		},

		draw({y, val}) {
			this.ctx.beginPath();
			this.ctx.strokeStyle = this.range.cssGrey(val);
			this.ctx.moveTo(0, y+0.5);
			this.ctx.lineTo(this.canvas.width, y+0.5);
			this.ctx.stroke();
		},
	}, ...args);
}

export function colQuality(...args) {
	return processor({
		points(data) {
			return detectGrid.scoreCols(data)
				.map((val, x) => ({x, val}))
				.filter(({v}) => v != 0);
		},

		initCtx() {
			this.ctx.setLineDash([20, 80]);
		},

		draw({x, val}) {
			this.ctx.beginPath();
			this.ctx.strokeStyle = this.range.cssGrey(val);
			this.ctx.moveTo(x+0.5, 0);
			this.ctx.lineTo(x+0.5, canvas.height);
			this.ctx.stroke();
		},
	}, ...args);
}

export function bestLines(...args) {
	return processor({
		points(data) {
			let rows = detectGrid.scoreRows(data);
			let cols = detectGrid.scoreCols(data);
			return [
				...detectGrid.bestPoints(rows).map(y => ({y, val: rows[Math.round(y)]})),
				...detectGrid.bestPoints(cols).map(x => ({x, val: cols[Math.round(x)]})),
			];
		},

		draw({x, y, val}) {
			this.ctx.beginPath();
			this.ctx.strokeStyle = this.range.cssColor(val);
			if (typeof x == "number") {
				this.ctx.moveTo(x+0.5, 0);
				this.ctx.lineTo(x+0.5, this.canvas.height);
			} else {
				this.ctx.moveTo(0, y+0.5);
				this.ctx.lineTo(this.canvas.width, y+0.5);
			}
			this.ctx.stroke();
		},
	}, ...args);
}

export function scoreRows(...args) {
	return processor({
		points(data) {
			let out = [];

			for (let y = detectGrid.EDGE_RANGE; y + detectGrid.EDGE_RANGE < data.height; y++) {
				for (let x = 0; x < data.width; x++) {
					out.push({
						x,
						y,
						val: detectGrid.scorePoint(data, y, x, (y, x) => x + y*data.width),
					});
				}
			}

			return out;
		},

		draw: drawGrey,
	}, ...args);
}

export function scoreCols(...args) {
	return processor({
		points(data) {
			let out = [];

			for (let x = detectGrid.EDGE_RANGE; x + detectGrid.EDGE_RANGE < data.width; x++) {
				for (let y = 0; y < data.height; y++) {
					out.push({
						x,
						y,
						val: detectGrid.scorePoint(data, x, y, (x, y) => x + y*data.width),
					});
				}
			}

			return out;
		},

		draw: drawGrey,
	}, ...args);
}
