import {averageInliers, nth, numericDesc, roundErrorFraction} from "./util.js";

function* probes(limit) {
	let step = Math.sqrt(limit) + 1 |0;

	for (let i = step; i < limit; i += step) {
		yield i;
	}
}

export const EDGE_RANGE = 8;

export function scorePoint(img, x, y, offset) {
	let p = img.data;

	let from = x - EDGE_RANGE;
	let to = x + EDGE_RANGE;

	let off = offset(x, y) * 4;
	let sum = p[off] + p[off+1] + p[off+2] + p[off+3];

	let lavg = 0;
	for (let i = from; i < x; i++) {
		let off = offset(i, y) * 4;
		lavg += p[off] + p[off+1] + p[off+2] + p[off+3];
	}
	lavg /= EDGE_RANGE;

	let ravg = 0;
	for (let i = x + 1; i <= to; i++) {
		let off = offset(i, y) * 4;
		ravg += p[off] + p[off+1] + p[off+2] + p[off+3];
	}
	ravg /= EDGE_RANGE;

	if ((sum < lavg) != (sum < ravg)) {
		return 0;
	}

	let ldiff = Math.abs(sum - lavg);
	let rdiff = Math.abs(sum - ravg);
	return Math.min(ldiff, rdiff);
}

function scoreLine(img, primaryLimt, secondaryLimit, offset) {
	let line = Array(primaryLimt).fill(0);
	let samples = [];
	for (let x = EDGE_RANGE; x < primaryLimt - EDGE_RANGE; x++) {
		samples.length = 0;

		for (let y of probes(secondaryLimit)) {
			samples.push(scorePoint(img, x, y, offset));
		}

		line[x] = averageInliers(samples, 0.5, 0.6);
	}

	return line;
}

export function scoreCols(img) {
	return scoreLine(img, img.width, img.height, (x, y) => x + y*img.width);
}

export function scoreRows(img) {
	return scoreLine(img, img.height, img.width, (y, x) => x + y*img.width);
}

export function bestPoints(line) {
	let threshold = nth([...line], Math.pow(line.length, 0.4)|0, numericDesc);

	let r = [];
	for (let i = 0; i < line.length; i++) {
		let maxv = line[i]
		if (maxv < threshold) continue;
		let maxi = i;

		while (i + 1 < line.length) {
			let v = line[++i]
			if (v < threshold) break;
			if (v <= maxv) continue;

			maxv = v;
			maxi = i;
		}

		if (maxi == 0 || maxi + 1 == line.length) {
			r.push(maxi);
			continue;
		}

		let iv = maxi;

		let pow = 1;
		maxv = Math.pow(maxv, pow)
		let left = Math.pow(line[maxi-1], pow);
		let right = Math.pow(line[maxi+1], pow);

		if ( left != right ) {
			maxi += 0.5 * (right - left) / (maxv - Math.min(left, right));
		}

		r.push(maxi);
	}
	return r;
}

function detectGrid1d(line) {
	let bestLines = bestPoints(line);

	let best = 0;
	let bestsize = Infinity;
	let bestpoint;
	let bestoff;

	for (let ai = 0; ai < bestLines.length-1; ai++) {
		let a = bestLines[ai];
		for (let bi = ai+1; bi < bestLines.length; bi++) {
			let b = bestLines[bi];

			let size = b - a;
			if (size < 7) continue;

			let distance = size;
			for (let i = 0; i < bestLines.length; i++) {
				if (i == ai || i == bi) continue;
				let p = bestLines[i];
				let da = Math.abs(p - a);
				let db = Math.abs(p - b);

				if (roundErrorFraction(da, size) > 0.01 || roundErrorFraction(db, size) > 0.01)
					continue;

				let d;
				if (da > db)
					d = da;
				else
					d = db;

				if (d < distance) continue;

				distance = d;
				size = d / Math.round(d / size);
			}

			let off = b % size;

			let avg = 0;
			for (let p = off; p < line.length; p += size) {
				let floor = p|0;
				let ceil = floor + 1;

				let v;
				if (ceil < line.length)
					v = line[floor] * (p - floor + 1) + line[ceil] * (ceil - p + 1);
				else
					v = line[floor];

				avg += v;
			}
			avg /= (line.length - off) / size + 1;

			// Prefer smaller grids. Otherwise the optimal grid would always be just the best two lines.
			//
			// This effectively says that we will subdivide a grid if the new points are at least half as good as the previous points.
			//
			// 2^0.6 == 1.516
			avg /= Math.pow(size, 0.6); // Prefer smaller grids.

			if (avg <= best) continue;

			best = avg;
			bestsize = size;
			bestpoint = b;
			bestoff = off;
		}
	}

	if (bestsize < 8) {
		// Generally a small size means that we couldn't find any meaningful signal. So assume that 7-8 is a failure. Ideally we could have better heuristics here in the future.
		return null;
	}

	if (bestoff*2 > bestsize) {
		bestoff -= bestsize;
	}

	return {
		cellSize: bestsize,
		offset: bestoff,
	};
}

/** Detect a grid from an image.
 *
 * Returns a best guess of the grid size. If a good guess could not be made `null` is returned.
 *
 * @param img {ImageData} An [ImageData](https://developer.mozilla.org/en-US/docs/Web/API/ImageData) object containing the image data.
 * @returns {{
 *   cellSize: {width: number, height: number},
 *   offset: {x: number, y: number},
 * }?}
 *   cellSize: An object the describes the estimated width and height of the grid cells.
 *   offset: An object the describes the estimated width and height of the grid cells.
 */
export default function detectGrid(img) {
	let cols = scoreCols(img);
	let xdim = detectGrid1d(cols);

	let rows = scoreRows(img);
	let ydim = detectGrid1d(rows);

	return {
		cellSize: {width: xdim.cellSize, height: ydim.cellSize},
		offset: {x: xdim.offset, y: ydim.offset},
	}
}
