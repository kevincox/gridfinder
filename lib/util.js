export function numericAsc(a, b) { return a - b }
export function numericDesc(a, b) { return b - a }

export function averageInliers(values, min, max) {
	values.sort(numericAsc);

	min = min*values.length |0;
	max = max*values.length |0;

	if (min == max) {
		return values[min];
	}

	let avg = 0;
	for (let i = min; i < max; i++) {
		avg += values[i];
	}
	avg /= max - min;

	return avg;
}

export function nth(values, n, sort=numericAsc) {
	values.sort(sort);
	return values[n];
}

export function percentile(values, p, sort=numericAsc) {
	return nth(values, values.length*p |0, sort);
}

export function roundError(v, multiple=1) {
	return v - Math.round(v / multiple) * multiple;
}

export function roundErrorFraction(v, multiple=1) {
	return Math.abs(roundError(v, multiple) / v);
}
