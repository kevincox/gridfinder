import Point from "./Point.js";
import * as debugDetectGrid from "./debugDetectGrid.js"
import {findGrid, detectGrid} from "./lib.js";

let points = [];

let instructions = document.getElementById("instructions");
let square = document.getElementById("square");

let canvas = document.getElementById("canvas");
let ctx = canvas.getContext("2d");

export function click(e) {
	let point = eventToCanvasCoords(e, canvas);
	points.push(point)

	drawPoint(point);

	console.log({points});

	if (points.length >= 2) {
		guessGrid();
	}
};

export function mousedown(e) {
	if (!points.length) return;

	let point = eventToCanvasCoords(e, canvas);

	let nearestPoint = 0;
	let nearestDistance = points[0].sub(point).length();
	for (let i = 1; i < points.length; i++) {
		let p = points[i]
		let d = p.sub(point).length();
		if (d < nearestDistance) {
			nearestPoint = i;
			nearestDistance = d;
		}
	}

	points.splice(nearestPoint, 1);

	if (points.length > 2) {
		guessGrid();
	} else {
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		points.forEach(drawPoint);
	}

	return false;
}

export function settings(e) {
	if (points.length > 2) {
		guessGrid();
	}
}

function eventToCanvasCoords(e, canvas) {
	let {top, bottom, left, right} = e.target.getBoundingClientRect();

	let screenX = e.clientX - left;
	let screenY = e.clientY - top;

	let screenWidth = right - left;
	let screenHeight = bottom - top;

	let canvasX = screenX / screenWidth * canvas.width;
	let canvasY = screenY / screenHeight * canvas.height;

	return new Point(canvasX, canvasY);
}

function drawPoint({x, y}, {color="red", size=null, thickness=null} = {}) {
	size = size || canvas.width / 200;
	thickness = thickness || size/2;

	ctx.beginPath();
	ctx.setLineDash([]);
	ctx.strokeStyle = color;
	ctx.lineWidth = thickness;
	ctx.arc(x, y, size, 0, 2 * Math.PI);
	ctx.stroke();
}

function guessGrid() {
	let issquare = square.checked;

	let shape = issquare? "square" : "rect";

	let g = findGrid(points, {shape});
	drawGrid(g);
}

function drawGrid({cellSize: {width, height}, offset}) {
	console.log({width, height, offset});

	ctx.clearRect(0, 0, canvas.width, canvas.height);

	instructions.textContent = `
		Grid cells are ${Math.round(width*100)/100}${width==height? "" : `x${Math.round(height*100)/100}`}px.
		Offset ${Math.round(offset.x)}, ${Math.round(offset.y)}.
		Map is ${Math.round(canvas.width/width)}x${Math.round(canvas.height/height)} cells.
	`;

	ctx.strokeStyle = "red";
	ctx.lineWidth = Math.max(1, width/40);

	ctx.beginPath();
	ctx.setLineDash([height/2]);
	for (let x = offset.x; x < canvas.width; x += width) {
		ctx.moveTo(x, offset.y - height/4);
		ctx.lineTo(x, canvas.height);
	}
	ctx.stroke();

	ctx.beginPath();
	ctx.setLineDash([width/2]);
	for (let y = offset.y; y < canvas.height; y += height) {
		ctx.moveTo(offset.x - width/4, y);
		ctx.lineTo(canvas.width, y);
	}
	ctx.stroke();

	points.forEach(drawPoint);
}

function detect() {
	let debug = new URLSearchParams(location.search).get("debug");

	ctx.drawImage(img, 0, 0);

	if (debug) {
		debugDetectGrid[debug](canvas);
	} else {
		let data = ctx.getImageData(0, 0, canvas.width, canvas.height);
		let r = detectGrid(data);
		drawGrid(r);
	}
}

let img = document.getElementById("bg");
function onLoad() {
	canvas.width = img.naturalWidth;
	canvas.height = img.naturalHeight;
	points = [];

	instructions.textContent = "Click intersections on the map to identify the grid.";

	try {
		detect();
	} catch (e) {
		if (!(e instanceof DOMException)) throw e;

		console.log("Could not read image data", e);
	}
}
img.addEventListener("load", onLoad);
if (img.complete && img.src) onLoad();

let upload = document.getElementById("upload");
upload.addEventListener("change", e => {
	img.src = URL.createObjectURL(e.target.files[0]);
});

window.addEventListener("paste", e => {
	if (!e.clipboardData.files.length) return;

	upload.files = e.clipboardData.files;
	img.src = URL.createObjectURL(upload.files[0]);
});

document.addEventListener("dragover", e => {
	e.preventDefault();
});

document.addEventListener("drop", e => {
	e.preventDefault();

	let url;
	if (e.dataTransfer.files.length) {
		upload.files = e.dataTransfer.files;
		img.src = URL.createObjectURL(upload.files[0]);
	} else if (url = e.dataTransfer.getData('URL')) {
		let f = document.getElementById("url");
		f.i.value = url;
		f.submit();
	}
});

+function(){
	let params = new URLSearchParams(location.search);
	let i = params.get("i");
	if (i) {
		img.src = i;
		document.getElementById("url").i.value = i;
	} else if (upload.files.length) {
		img.src = URL.createObjectURL(upload.files[0]);
	}
}();
