import Point from "./Point.js";

/** Find the grid that best matches the provided points.
 *
 * @param points {{x: number, y: number}[]} A list of points.
 * @param options
 * @returns {{
 *   cellSize: {width: number, height: number},
 *   offset: {x: number, y: number},
 * }}
 *   cellSize: An object the describes the estimated width and height of the grid cells.
 *   offset: An object the describes the estimated width and height of the grid cells.
 */
export function findGrid(points, {
	/** If `true` the `offset` field of the output will be set.
	 *
	 * It may be slightly more efficient to calculate the offset at the same time as finding the grid. Therefore if you need both leave this as true. However if you don't need the offset pass false.
	 *
	 * Note: The offset can always be calculated as `findOffset({points, cellSize: findGrid(points, ...).cellSize)`.
	 */
	calculateOffset = true,

	/** The shape of grid to look for.
	 *
	 * - "square": Returns a square grid. Always `cellSize.width == cellSize.height`.
	 * - "rect": Similar to "square" except the width and height may vary. As well as being useful for truly rectangular grids this can also be used for skewed images.
	 *
	 *   Note: For square grids "square" will produce better results due to the additional assumptions it can make. It is recommended only to use "rect" if you expect the grid can be non-square. If "square" is the more common case it may make sense to default to it and allow the user to select "rect" for the uncommon case.
	 *
	 */
	shape = "square",
} = {}) {
	let width, height;
	let offsetX, offsetY;

	if (shape == "square") {
		let {cellSize, offset: {x, y}} = findGrid2d(points);
		width = height = cellSize;
		offsetX = x;
		offsetY = y;
	} else if (shape == "rect"){
		let x = findGrid1d(points.map(p => p.x));
		width = x.cellSize;
		offsetX = x.offset;

		let y = findGrid1d(points.map(p => p.y));
		height = y.cellSize;
		offsetY = y.offset;
	} else {
		throw Error(`Unknown shape: ${shape}`)
	}

	return {
		cellSize: {width, height},
		offset: calculateOffset ? {x: offsetX, y: offsetY} : null,
	}
}

/** Find the offset of the grid.
 *
 * @returns {{x: number, y: number}} The estimated origin of the grid.
 */
export function findOffset({
	/// The size of cells.
	cellSize,

	/// A list of known intersections.
	points,

	/// A point that is near an intersection. If not provided a suitable reference will be selected.
	reference = null,
}) {
	reference = reference || points.reduce((a, b) => ({x: Math.min(a.x, b.x), y: Math.min(a.y, b.y)}));

	if (typeof cellSize == "number") {
		cellSize = {width: cellSize, height: cellSize};
	}

	return {
		x: findOffset1d(cellSize.width, points.map(p => p.x), reference.x),
		y: findOffset1d(cellSize.height, points.map(p => p.y), reference.y),
	};
}

function roundError1d(cellSize, point) {
	return point - Math.round(point/cellSize)*cellSize;
}

function findOffset1d(cellSize, points, reference) {
	let totalError = points.reduce((total, point) => {
		return total + roundError1d(cellSize, point - reference)
	}, 0);
	return totalError / points.length + roundError1d(cellSize, reference);
}

export function findGrid1d(points) {
	let min = Math.min(...points);
	let max = Math.max(...points);
	let length = max - min;

	let minErr = Infinity;
	let bestCellSize;
	let bestOffset;
	for (let cells = 1; cells < length/8; cells++) {
		let cellSize = length / cells;

		let off = findOffset1d(cellSize, points, min);
		let err = 0;
		for (let point of points) {
			err += Math.pow(roundError1d(cellSize, point-min) / cellSize, 2);
		}

		if (err < minErr) {
			minErr = err;
			bestCellSize = cellSize;
			bestOffset = off;
		}
	}

	return {
		cellSize: bestCellSize,
		offset: bestOffset,
	}
}

function findOffset2d(cellSize, points, reference) {
	return new Point(
		findOffset1d(cellSize, points.map(p => p.x), reference.x),
		findOffset1d(cellSize, points.map(p => p.y), reference.y));
}

function findGrid2d(points) {
	let left = points[0].x;
	let right = points[0].x;
	let top = points[0].y;
	let bottom = points[0].y;

	for (let {x, y} of points.slice(1)) {
		if (x < left) left = x;
		if (x > right) right = x;
		if (y < top) top = y;
		if (y > bottom) bottom = y;
	}

	let width = right - left;
	let height = bottom - top;
	let minDim = Math.min(width, height);
	let maxDim = Math.max(width, height);

	let origin = new Point(left, top);

	points = points.map(({x, y}) => new Point(x, y));

	let minErr = Infinity;
	let bestCellSize;
	let bestOffset;
	for (let cells = 1; cells < minDim/8; cells++) {
		let minCellSize = minDim / cells;
		let maxCells = Math.round(maxDim/minCellSize);
		let cellSize = maxDim/maxCells;

		let off = findOffset2d(cellSize, points, origin);
		let err = 0;
		for (let point of points) {
			err += point.sub(off).roundError(cellSize).div(cellSize).lengthSquared();
		}

		if (err < minErr) {
			minErr = err;
			bestCellSize = cellSize;
			bestOffset = off;
		}
	}

	return {
		cellSize: bestCellSize,
		offset: bestOffset,
	}
}
