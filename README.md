# Gridfinder

A tool to quickly identify the grid in a map with just a couple of clicks.

- https://gridfinder.kevincox.ca/
- https://gridfinder.kevincox.ca/faq.html

## Algorithm

The algorithm is explained at https://kevincox.ca/2020/11/06/gridfinder/

## Library Use

```sh
$ npm install --save-prod gridfinder
```

```javascript
import {findGrid} from "gridfinder";
```

See [lib.js](lib/lib.js) for available API.

Warning: The only supported import target is `"gridfinder"` (which maps to [lib.js](lib/lib.js)). Everything available from that path is a public, stable API. Anything imported from other files is non-public and will be changed without regard for external users.
