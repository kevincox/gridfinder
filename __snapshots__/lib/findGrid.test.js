exports['findGrid 256 farmhouse 1 1'] = {
  "cellSize": {
    "width": 138.04796430563303,
    "height": 138.04796430563303
  },
  "offset": {
    "x": 8.953337051496268,
    "y": 70.80786793400029
  }
}

exports['findGrid 256 farmhouse bad +1 1'] = {
  "cellSize": {
    "width": 8.28860869891713,
    "height": 8.28860869891713
  },
  "offset": {
    "x": -4.139983052459229,
    "y": -4.144304349458196
  }
}

exports['findGrid 256 farmhouse bad +2 1'] = {
  "cellSize": {
    "width": 255.01325932778298,
    "height": 255.01325932778298
  },
  "offset": {
    "x": 20.997841504778442,
    "y": 17.95427024556227
  }
}

exports['findGrid 256 farmhouse bad +a lot 1'] = {
  "cellSize": {
    "width": 255.84212149244524,
    "height": 255.84212149244524
  },
  "offset": {
    "x": 6.207256655360515,
    "y": 6.110142328299374
  }
}

exports['findGrid 256 farmhouse bad 1'] = {
  "cellSize": {
    "width": 8.28860869891713,
    "height": 8.28860869891713
  },
  "offset": {
    "x": -4.140417879597938,
    "y": -4.144304349458214
  }
}

exports['findGrid 256 farmhouse min 1'] = {
  "cellSize": {
    "width": 53.6535700300222,
    "height": 53.6535700300222
  },
  "offset": {
    "x": 21.929252055866073,
    "y": -11.780323912012099
  }
}

exports['findGrid 60 perspective_inn 1 1'] = {
  "cellSize": {
    "width": 59.54866859941837,
    "height": 59.54866859941837
  },
  "offset": {
    "x": 27.630823630663443,
    "y": 16.16672531810564
  }
}

exports['findGrid 60 perspective_inn rectangular 1'] = {
  "cellSize": {
    "width": 60.25889967637539,
    "height": 112.98508555657122
  },
  "offset": {
    "x": -18.077669902912533,
    "y": -51.21990545231233
  }
}

exports['findGrid 60 perspective_inn refine_grid 1'] = {
  "cellSize": {
    "width": 125.74098057354308,
    "height": 125.74098057354308
  },
  "offset": {
    "x": -32.72710453284037,
    "y": 54.04341830541574
  }
}

exports['findGrid 60 perspective_inn refine_grid half 1 1'] = {
  "cellSize": {
    "width": 62.87049028677154,
    "height": 62.87049028677154
  },
  "offset": {
    "x": 29.798889916743384,
    "y": -7.406024207960582
  }
}

exports['findGrid 60 perspective_inn refine_grid half 2 1'] = {
  "cellSize": {
    "width": 62.87049028677154,
    "height": 62.87049028677154
  },
  "offset": {
    "x": 29.971137835337277,
    "y": -9.12848472302419
  }
}

exports['findGrid perfect clicks 1 1'] = {
  "cellSize": {
    "width": 100,
    "height": 100
  },
  "offset": {
    "x": 0,
    "y": 0
  }
}

exports['findGrid perfect clicks 2 1'] = {
  "cellSize": {
    "width": 72,
    "height": 72
  },
  "offset": {
    "x": 0,
    "y": 0
  }
}

exports['findGrid rectangular offset 1'] = {
  "cellSize": {
    "width": 10,
    "height": 30
  },
  "offset": {
    "x": -5,
    "y": 5
  }
}

exports['findGrid rectangular three 1'] = {
  "cellSize": {
    "width": 10,
    "height": 30
  },
  "offset": {
    "x": 0,
    "y": 0
  }
}

exports['findGrid rectangular two 1'] = {
  "cellSize": {
    "width": 10,
    "height": 30
  },
  "offset": {
    "x": 0,
    "y": 0
  }
}

exports['findGrid very good clicks 1'] = {
  "cellSize": {
    "width": 104,
    "height": 104
  },
  "offset": {
    "x": 2,
    "y": -5.333333333333333
  }
}
