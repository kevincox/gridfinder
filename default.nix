{
	nixpkgs ? import <nixpkgs> {},
	napalm ? nixpkgs.pkgs.callPackage (
		fetchTarball "https://github.com/nix-community/napalm/archive/master.tar.gz"
	) {},
	kevincox-web-compiler ? builtins.storePath (nixpkgs.lib.fileContents (builtins.fetchurl  "https://kevincox.gitlab.io/kevincox-web-compiler/bin.nixpath")),
}: with nixpkgs; let
		src = pkgs.nix-gitignore.gitignoreSource [
			"*"
			"!src/"
			"!lib/"
		] ./.;
in rec {
	gridfinder = napalm.buildPackage
		(pkgs.nix-gitignore.gitignoreSource [
			"*.nix"
			".*"
		] ./.)
		{
			buildInputs = with pkgs; [
				cairo
				libjpeg
				pango
				pkg-config
				python3
			];

			# Canvas has a very opinionated check for libjpeg, override it.
			GYP_DEFINES = "with_jpeg=true";

			doCheck = true;
			checkPhase = "npm test";
		};

	www = pkgs.runCommand "gridfinder-www" {} ''
		${kevincox-web-compiler}/bin/kevincox-web-compiler \
			--asset-mode=hash \
			${src}/src/ $out
	'';
}
